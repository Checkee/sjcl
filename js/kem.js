var Ksec;
var Kpub;
var tag;

function keygen(){
    Kpair = sjcl.ecc.elGamal.generateKeys();
    Kpub = Kpair.pub;
    Ksec = Kpair.sec;
}

function kem(){
    var z = Kpub.kem(0);
    var K = z.key;
    tag = z.tag;

    $("#tag").val(sjcl.codec.base64.fromBits(tag));
    $("#ka").val(sjcl.codec.base64.fromBits(K));
}


function unkem(){
    var K = Ksec.unkem(tag);
    $("#kb").val(sjcl.codec.base64.fromBits(K));
}

