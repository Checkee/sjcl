
function genKeys(){
    var numCurve = $("#curve").find(":radio:checked").val();

    var curve = sjcl.ecc.curves['c'+numCurve];

    var sec = sjcl.bn.random(curve.r, 0);

    var pub = curve.G.mult(sec);

    var Kpub = new sjcl.ecc.elGamal.publicKey(curve, pub);
    var Ksec  = new sjcl.ecc.elGamal.secretKey(curve, sec);


    //var Kpair = sjcl.ecc.elGamal.generateKeys(curve, 6, sec);
    //Kpair = sjcl.ecc.elGamal.generateKeys(); //p256, 6, random

    $("#order").val(curve.r);
    $("#sec").val(sec);
    $("#pubx").val(pub.x);
    $("#puby").val(pub.y);
    $("#ksecexp").val(sjcl.codec.hex.fromBits(Ksec.get()));
    $("#kpubx").val(sjcl.codec.hex.fromBits(Kpub.get().x));
    $("#kpuby").val(sjcl.codec.hex.fromBits(Kpub.get().y));
}