
var key;
var cipher;
var iv;

function testPBKDF2(){
    var passwd = $("#passwd").val();
    var salt = sjcl.random.randomWords(2, 0);
    key = sjcl.misc.pbkdf2(passwd, salt, 1000, 256);

    var key64 = sjcl.codec.base64.fromBits(key);
    var salt64 = sjcl.codec.base64.fromBits(salt);

    $("#key").val(key64);
    $("#salt").val(salt64);
}

function encAES(){
    var message = sjcl.codec.utf8String.toBits($("#message").val());

    var aes = new sjcl.cipher.aes(key); //key 256 bits (8 words)
    iv = sjcl.random.randomWords(8, 0);

    cipher = sjcl.mode.ccm.encrypt(aes, message, iv);

    var iv64 = sjcl.codec.base64.fromBits(iv);
    var cipher64 = sjcl.codec.base64.fromBits(cipher);

    $("#iv").val(iv64);
    $("#cipher").val(cipher64);
}

function decAES(){
    var aes = new sjcl.cipher.aes(key);

    var plaintext = sjcl.mode.ccm.decrypt(aes, cipher, iv);

    var plaintextUTF8 = sjcl.codec.utf8String.fromBits(plaintext);

    $("#plaintext").val(plaintextUTF8);
}