var Ksec;
var Kpub;

var cipher;

function keygen(){
    Kpair = sjcl.ecc.elGamal.generateKeys();
    Kpub = Kpair.pub;
    Ksec = Kpair.sec;
}

function encrypt(){
    var message = $("#message").val();

    cipher = sjcl.encrypt(Kpub, message);

    $("#cipher").val(jQuery.parseJSON(cipher).ct);
}

function decrypt(){
    var message = sjcl.decrypt(Ksec, cipher);

    $("#plaintext").val(message);
}