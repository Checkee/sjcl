var K;

function PBKDF2() {
    var passwd = "toto";
    var salt = sjcl.random.randomWords(2, 0);
    K = sjcl.misc.pbkdf2(passwd, salt, 1000, 256);
}


function hmac(){
    var message = $("#message").val();

    var hmac = new sjcl.misc.hmac(K, sjcl.hash.sha256);
    var h = hmac.encrypt(message);

    $("#hmac").val(sjcl.codec.base64.fromBits(h));
}

function verify(){

    var message = $("#message").val();
    var h = sjcl.codec.base64.toBits($("#hmac").val());

    var hmac = new sjcl.misc.hmac(K, sjcl.hash.sha256);

    $("#verif").val(sjcl.codec.base64.fromBits(hmac.encrypt(message)));

}