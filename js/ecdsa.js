var Kpub;
var Ksec;


function keygen(){
    var numCurve = $("#curve").find(":radio:checked").val();

    var curve = sjcl.ecc.curves['c'+numCurve];

    var sec = sjcl.bn.random(curve.r, 0);

    var pub = curve.G.mult(sec);

    Kpub = new sjcl.ecc.ecdsa.publicKey(curve, pub);
    Ksec  = new sjcl.ecc.ecdsa.secretKey(curve, sec);

}

function sign(){

    var message = $("#message").val();

    var h = sjcl.hash.sha256.hash(message);

    var s = Ksec.sign(h, 0, false);

    $("#hash").val(sjcl.codec.base64.fromBits(h));
    $("#sig").val(sjcl.codec.base64.fromBits(s));

}

function verify(){

    var message = $("#message").val();
    var s = sjcl.codec.base64.toBits($("#sig").val());

    var h = sjcl.hash.sha256.hash(message);

    var v = true;
    try {
        Kpub.verify(h, s, false);
    }catch (err){
        v = false;
    }

    $("#verif").val(v);
}