var Kasec;
var Kapub;

var Kbsec;
var Kbpub;

function aliceKeygen(){
    var Kpair = sjcl.ecc.elGamal.generateKeys();
    Kapub = Kpair.pub;
    Kasec = Kpair.sec;

    $("#kapub").val(sjcl.codec.base64.fromBits(Kapub.get().x));
    $("#kasec").val(sjcl.codec.base64.fromBits(Kasec.get()));
}

function bobKeygen(){
    var Kpair = sjcl.ecc.elGamal.generateKeys();
    Kbpub = Kpair.pub;
    Kbsec = Kpair.sec;

    $("#kbpub").val(sjcl.codec.base64.fromBits(Kbpub.get().x));
    $("#kbsec").val(sjcl.codec.base64.fromBits(Kbsec.get()));
}

function dh(){
    var Ka = Kasec.dh(Kbpub);
    var Kb = Kbsec.dh(Kapub);

    $("#ka").val(sjcl.codec.base64.fromBits(Ka));
    $("#kb").val(sjcl.codec.base64.fromBits(Kb));
}
