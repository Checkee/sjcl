
var cipher;

function encryptDemo() {
    var message = $("#message").val();
    var passwd = $("#passwd").val();
    cipher = sjcl.encrypt(passwd, message);
    printCipher(cipher);
}

function decryptDemo(){
    var passwd = $("#passwd").val();
    var message = sjcl.decrypt(passwd, cipher);
    $("#decrypted_message").val(message);
}

function printCipher(cipher){
    var jsonCipher = jQuery.parseJSON(cipher);

    $("#ks").val(jsonCipher.ks);
    $("#iter").val(jsonCipher.iter);
    $("#salt").val(jsonCipher.salt);
    $("#iv").val(jsonCipher.iv);
    $("#cipher").val(jsonCipher.ct);

}