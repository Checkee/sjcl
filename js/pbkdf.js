
function testPBKDF2(){
    var passwd = $("#passwd").val();
    var salt = sjcl.random.randomWords(2, 0);
    var key = sjcl.misc.pbkdf2(passwd, salt, 100000, 256);

    var key64 = sjcl.codec.base64.fromBits(key);
    var salt64 = sjcl.codec.base64.fromBits(salt);

    $("#key").val(key64);
    $("#salt").val(salt64);
}